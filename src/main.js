import Vue from 'vue'
import App from './App.vue'
import store from './store'
import vuetify from './plugins/vuetify';
import VueGtag from "vue-gtag";

Vue.config.productionTip = false

Vue.use(require('vue-cookies'))
Vue.use(VueGtag, {
  config: { id: "G-QJ2XKT0YEJ" },
  includes: [
    {id: 'UA-153831425-1'}
  ]
});

new Vue({
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
