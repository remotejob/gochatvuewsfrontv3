const state = {

    answermsg: '',
    askmsg: '',

}
const getters = {
    getAnswermsg: state => {
        return state.answermsg
    },
    getAskmsg: state => {
        return state.askmsg
    }   

}
const mutations = {

    setAnswerMsg(state, payload) {
        state.answermsg = payload.answer
    },
    setAskMsg(state, msg) {
        state.askmsg = msg
    },


}
const actions = {

    setAnswerMsg(context, payload) {
        context.commit('setAnswerMsg', payload)

    },
    setAskMsg(context, msg) {
        context.commit('setAskMsg', msg)

    },



}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters

}