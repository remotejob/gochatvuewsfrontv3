const state = {

    menu: "HelloWorld",
    loaded: false,
    selectedImg: {},

}

const getters = {
    getSelectedImg: state => {
        return state.selectedImg
    }

}

const mutations = {

    setLoaded(state, val) {
        state.loaded = val
    },

    makeSelectedImg(state, val) {

        state.selectedImg = val


    },

    setMenu(state, val) {
        state.menu = val
    },

}
const actions = {

    setLoaded(context, val) {
        context.commit('setLoaded', val)
    },

    setSelectedImg(context, val) {

        context.commit('makeSelectedImg', val)

    },

    setMenu(context, val) {
        context.commit('setMenu', val)
    },

}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters

}