const state = {
    history: [],
    historyOK: false,
    gethistory: false,

}

const mutations = {
    historyOK(state, msg) {

        state.historyOK = msg

    },
    getHistory(state, msg) {

        state.gethistory = msg

    },
    setHistory(state, msg) {

        state.history = msg
        state.gethistory = false

    },   

}
const actions = {

    historyOK(context, msg) {
        context.commit('historyOK', msg)

    },
    getHistory(context, msg) {
        context.commit('getHistory', msg)

    },
    setHistory(context, msg) {

        context.commit('setHistory', msg) 

    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions

}