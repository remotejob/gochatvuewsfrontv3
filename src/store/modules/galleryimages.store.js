const state = {

    galleryimgs: [],
    setimages: false
}

const mutations = {

    getimages(state, val) {

        state.getimages = val
    },
    setGalleryImgs(state, val) {
        state.galleryimgs = val
    },


}
const actions = {

    getimages(context, val) {
        context.commit('getimages', val)
    },
    setGalleryImgs(context, val) {

        context.commit('setGalleryImgs', val)
    },

}

export default {
    namespaced: true,
    state,
    mutations,
    actions

}