import 'es6-promise/auto'
import Vuex from 'vuex'
import Vue from 'vue'
import Common from './modules/common.store';
import Head from './modules/head.store';
import Galleryimages from './modules/galleryimages.store';
import Chat from './modules/chat.store'
import History from './modules/history.store'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

    wsserverurl: process.env.VUE_APP_WSSERVER,

  },

  modules: {

    Common,
    Head,
    Galleryimages,
    Chat,
    History,

  }
});