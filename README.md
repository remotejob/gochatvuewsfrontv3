# gochatvuewsfrontv3

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

Chat
https://stackoverflow.com/questions/62641318/simple-chat-layout-with-vuetify-chat-history-growing-from-bottom-to-top

https://blog.bitsrc.io/4-best-practices-for-large-scale-vue-js-projects-9a533450bdb2
<slot> modules vuex

https://github.com/championswimmer/vuex-persist
https://github.com/localForage/localForage

Start development branch

important:
nvm use default (v15.12.0)
nvm alias default v15.12.0
npm install -g @vue/cli
vue --version
@vue/cli 4.5.12

https://gazepass.com/  NO PASS !??